import { mount, Wrapper, createLocalVue } from "@vue/test-utils";
import Series from "@/views/Series.vue";
// import routes from '@/router';
const localVue = createLocalVue();

const wrapper: Wrapper<Series> = mount(Series, {
  localVue,
  mocks: {
    $route: {
      params: {
        id: 1
      }
    }
  },
  stubs: [
    "f7-page",
    "f7-navbar",
    "f7-nav-right",
    "f7-nav-title",
    "f7-swiper",
    "f7-swiper-slide",
    "f7-card",
    "f7-card-content",
    "f7-card-footer",
    "f7-block",
    "f7-block-title",
    "f7-link",
    "f7-row",
    "f7-col",
    "vue-core-video-player",
    "f7-nav-left",
    "font-awesome-icon"
  ]
});
describe("Series.vue", () => {
  it("Shows First Index", done => {
    setTimeout(() => {
      const loaded = wrapper.vm.$data.loaded;
      done();
      expect(loaded).toBe(true);
    }, 2000);
  });
});
