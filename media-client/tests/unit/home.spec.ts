import { mount, Wrapper, createLocalVue } from "@vue/test-utils";
import Series from "@/Transfer/Series";
import Home from "@/views/Home.vue";
const localVue = createLocalVue();
const wrapper: Wrapper<Home> = mount(Home, {
  localVue,
  stubs: [
    "f7-page",
    "f7-navbar",
    "f7-nav-right",
    "f7-nav-title",
    "f7-swiper",
    "f7-swiper-slide",
    "f7-card",
    "f7-card-footer",
    "f7-block",
    "f7-row",
    "f7-col",
    "font-awesome-icon"
  ]
});
describe("Home.vue", () => {
  it("Shows most recent trailers", done => {
    setTimeout(() => {
      const mrecs: Series[] = wrapper.vm.$data.mrecs;
      expect(mrecs.length).toBeTruthy();
      done();
    }, 1000);
  });
  it("Shows series page", done => {
    setTimeout(() => {
      const series: Series[] = wrapper.vm.$data.series;
      done();
      expect(series.length).toBeTruthy();
    }, 1000);
  });
  it("Shows page count", done => {
    setTimeout(() => {
      const pagecount: number = wrapper.vm.$data.pagecount;
      done();
      expect(pagecount).toBeGreaterThan(1);
    }, 1000);
  });
  it("Next Button Works", done => {
    const nextBtn = wrapper.find("#next-btn");
    nextBtn.trigger("click");
    setTimeout(() => {
      done();
      expect(wrapper.vm.$data.currentPage).toBeGreaterThan(1);
    }, 2000);
  });
});
