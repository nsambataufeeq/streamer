import Series from "@/Transfer/Series";
import { mount, Wrapper } from "@vue/test-utils";
import Search from "@/views/Search.vue";
const wrapper: Wrapper<Search> = mount(Search, {
  stubs: [
    "f7-page",
    "f7-navbar",
    "f7-nav-right",
    "f7-nav-title",
    "f7-block",
    "f7-block-title",
    "f7-list",
    "f7-list-item",
    "f7-searchbar",
    "font-awesome-icon"
  ]
});
describe("Search.vue", () => {
  it("Show series index", done => {
    setTimeout(() => {
      const series: Series[] = wrapper.vm.$data.series;
      done();
      expect(series.length).toBeTruthy();
    }, 2000);
  });
});
