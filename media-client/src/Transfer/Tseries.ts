import Series from "./Series";
import TvShow from "./TvShow";
import Related from "./Related";
class Tseries extends Series {
  seasons: number;
  plot: string;
  related: Related[] = [];
  constructor(data: TvShow) {
    super(data);
    this.seasons = data.seasons as number;
    this.plot = data.plot as string;
    const related: TvShow[] = data.related as TvShow[];
    this.related = related.map(function(tvshow: TvShow) {
      return new Related(tvshow);
    });
  }
}
export default Tseries;
