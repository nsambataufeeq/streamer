interface TvShow {
  id?: number;
  name?: string;
  releaseDate?: string;
  url?: string;
  seasons?: number;
  plot?: string;
  related?: TvShow[];
}
export default TvShow;
