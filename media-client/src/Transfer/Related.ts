import TvShow from "./TvShow";
class Related {
  id: number;
  name: string;
  url: string;
  constructor(data: TvShow) {
    this.id = data.id as number;
    this.name = data.name as string;
    const url: string = data.url as string;
    this.url = process.env.VUE_APP_MEDIA + "/" + url;
  }
}
export default Related;
