import Related from "./Related";
import TvShow from "./TvShow";
class Series extends Related {
  year: number;
  constructor(data: TvShow) {
    super(data);
    const date = new Date(data.releaseDate as string);
    this.year = date.getFullYear();
  }
}
export default Series;
