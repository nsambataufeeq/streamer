<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TvShow;
class TvShowGenre extends Model
{
    //
    protected $table="tvshow_genres";
    protected $fillable=[
        'genre_id',
        'tvshow_id'
    ];
    public function tvshow(){
        return $this->belongsTo(TvShow::class);
    }
}
