<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Genre;
class TvShow extends Model
{
    //
    protected $table = "tvshows";
    public function genres(){
        return $this->belongsToMany(Genre::class,'tvshow_genres','tvshow_id','genre_id');
    }

}
