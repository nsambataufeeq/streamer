<?php
namespace App\Transfer;
use App\Transfer\Series;
use App\Models\TvShow;
use App\Models\TvShowGenre;
use App\Transfer\Related;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Tseries extends Series{
    public int $seasons;
    public string $plot;
    public array $related;
    public function __construct(TvShow $tvshow){
        parent::__construct($tvshow);
        $this->plot = $tvshow->plot;
        $this->seasons = $tvshow->seasons;
        $genres = $tvshow->genres->pluck('id')->toArray();
        $series = DB::table('tvshow_genres')->select('tvshow_id',DB::raw('count(*) as matches'))
        ->whereIn('genre_id',$genres)
        ->groupBy('tvshow_id')
        ->orderByDesc('matches')
        ->limit(5)
        ->get()->pluck('tvshow_id');
        // Log::info($series);

        // $series = TvShowGenre::whereIn('genre_id',$genres)->get()->pluck('tvshow_id')->unique();
        $series = $series->map(function($member){
            return intval($member);
        });
        $series = $series->filter(function($result)use($tvshow){
            return $result!=$tvshow->id;
        });
        $tseries = TvShow::whereIn('id',$series->toArray())->get()->mapInto(Related::class);
        $this->related = array_values($tseries->toArray());
    }
}
