<?php
namespace App\Transfer;
use App\Models\TvShow;
class Related{
    public int $id;
    public string $name;
    public string $url;
    public function __construct(TvShow $tvshow){
        $this->id = $tvshow->id;
        $this->name = $tvshow->name;
        $this->url = $tvshow->url;
    }
}
