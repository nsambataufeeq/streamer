<?php
namespace App\Transfer;
use App\Models\TvShow;
use App\Transfer\Related;
class Series extends Related{
    public string $releaseDate;
    public function __construct(TvShow $tvshow)
    {
        parent::__construct($tvshow);
        $this->releaseDate = $tvshow->release_date;
    }
}
