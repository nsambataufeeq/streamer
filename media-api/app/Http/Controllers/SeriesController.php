<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TvShow;
use App\Transfer\Tseries;
use Illuminate\Support\Facades\Log;

class SeriesController extends Controller
{
    //
    public function index(Request $request){
        $seriesId = $request->input("seriesId");
        $tvshow = TvShow::find($seriesId);
        $tvseries = new Tseries($tvshow);
        return response()->json($tvseries);
    }
}
