<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\TvShow;
use App\Transfer\Series;
use Illuminate\Support\Facades\Log;
use stdClass;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private array $columns = ['id','name','url','release_date'];
    private int $pglimit = 5;
    const PERLIM = 4;

    public function getPageCount(Request $request){
        $pages = ceil(TvShow::count()/self::PERLIM);
        $tmp  = new stdClass;
        $tmp->pages = $pages;
        return response()->json($tmp);
    }

    public function index(Request $request){
        $pageNo = (int) $request->input('pageNo');
        $offset = ($pageNo-1)*self::PERLIM;
        $tvshows = TvShow::offset($offset)->limit($this->pglimit-1)->get($this->columns);
        $series = $tvshows->map(function($tvshow){ return new Series($tvshow);});
        return response()->json($series);
    }
    //
    public function mostRecent(){
        $tvshows = TvShow::orderByDesc('buy_date')->limit($this->pglimit)->get($this->columns);
        $series = $tvshows->map(function($tvshow){ return new Series($tvshow);});
        return response()->json($series);
    }
}
