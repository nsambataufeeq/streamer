<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\TvShow;
use App\Transfer\Series;

class SearchController extends Controller{
    private array $columns = ['id','name','url','release_date'];
    public function index(){
        $tvshows = TvShow::all()->take(10);
        $series = $tvshows->map(function($tvshow){return new Series($tvshow);});
        return response()->json($series);
    }
    public function find(Request $request){
        $showName =$request->input("showName");
        $tvshows = TvShow::where('name','like','%'.$showName.'%')->get($this->columns);
        $series = $tvshows->map(function($tvshow){ return new Series($tvshow);});
        return response()->json($series);
    }

}
