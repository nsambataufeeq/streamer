<?php

class HomeTest extends TestCase
{
    public int $currentPage;
    public function __construct()
    {
        parent::__construct();
        $this->currentPage = 1;
    }
    public function testgetPageCount(){
        $response = $this->json('GET',"/home/page_count");
        $response->assertResponseOk();
        $pages = $response->response->decodeResponseJson();
        $this->assertGreaterThan(0,$pages['pages']);
    }
    public function testIndex()
    {
        $response = $this->json('GET', '/home/index',['pageNo'=>$this->currentPage]);
        $response->assertResponseOk();
        $series = $response->response->decodeResponseJson();
        $series = collect($series);
        $this->assertGreaterThan(0, $series->count(), "Shows series page");
    }
    public function testMostRecent()
    {
        $response = $this->json('GET', '/home/most_recent');
        $response->assertResponseOk();
        $series = $response->response->decodeResponseJson();
        $series = collect($series);
        $this->assertGreaterThan(0, $series->count(), "Shows most recent");
    }
    public function testNextPage()
    {
        $this->currentPage += 1;
        $response =$this->json('GET', '/home/index', ['pageNo' => $this->currentPage]);
        $response->assertResponseOk();
        $series = $response->response->decodeResponseJson();
        $series = collect($series);
        $this->assertGreaterThan(0,$series->count());
    }
}
