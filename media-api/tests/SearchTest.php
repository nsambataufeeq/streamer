<?php

class SearchTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->json('GET','/search/index');
        $response->assertResponseOk();
        $series = $response->response->decodeResponseJson();
        $tvshows = collect($series);
        $this->assertGreaterThan(0,$tvshows->count());
    }
    public function testFind(){
        $response = $this->json('GET','/search/find',['showName'=>'Boys']);
        $response->assertResponseOk();
        $series = $response->response->decodeResponseJson();
        $tvshows = collect($series);
        $this->assertGreaterThan(0,$tvshows->count());
    }
}
