<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SeriesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->json("GET","/series/index",['seriesId'=>1]);
        $response->assertResponseOk();
        $tseries = $response->response->decodeResponseJson();
        $this->assertGreaterThan(0,strlen($tseries['name']));
    }
}
