<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/home/page_count','HomeController@getPageCount');
$router->get('/home/most_recent','HomeController@mostRecent');
$router->get('/home/index','HomeController@index');

$router->get('/series/index','SeriesController@index');

$router->get("/search/index",'SearchController@index');
$router->get("/search/find","SearchController@find");
// $router->get('/home/search','HomeController@search');
